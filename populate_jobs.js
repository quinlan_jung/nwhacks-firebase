  // CREATE A REFERENCE TO FIREBASE
  var messagesRef = new Firebase('brilliant-heat-2461.firebaseIO.com/jobs');
    //var messagesRef = new Firebase('favorsnw.firebaseio.com/jobs');

  function generateRandomJobJson(){
    var jobId = generateRandomJobId();
    var description = chance.sentence({words: 10});
    var creatorUserId = generateRandomUserId();
    var spotsAvailable = chance.integer({min: 1, max: 5});
    var spotUserIds = generateRandomArray(chance.integer({min: 1, max: spotsAvailable}), getUserIdMinRange(), getUserIdMaxRange());
    var pendingSpotIds = generateRandomArray(chance.integer({min: 1, max: 5}), getUserIdMinRange(), getUserIdMaxRange());

    return {"jobId" : jobId , "description" : description, "creatorUserId" : creatorUserId, "spotsAvailable" : spotsAvailable, "spotUserIds" : spotUserIds, "pendingSpotIds" : pendingSpotIds};
  }

  function generateRandomJobJsonArray(n){
    jobs = []; // For printing to console later
    for (i = 0; i < n; i++){
      console.log("creating job" + i + " "+n);
      var jobJson = generateRandomJobJson();
      jobs.push(jobJson);
      messagesRef.push(jobJson);
    }
    return jobs;
  }

console.log("Creating jobs");
console.log(generateRandomJobJsonArray(5));
console.log("Successfully created jobs");
