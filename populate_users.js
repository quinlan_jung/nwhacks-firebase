  // CREATE A REFERENCE TO FIREBASE
  var messagesRef = new Firebase('brilliant-heat-2461.firebaseIO.com/users');
  //var messagesRef = new Firebase('favorsnw.firebaseio.com/users');

  function generateRandomUserJson(){
    var userId = generateRandomUserId();
    var firstName = chance.first();
    var lastName = chance.last();
    var email = chance.email();
    var password = chance.word();
    var linkedInURL = "www.linkedin.com/"+firstName;
    var teachingRating = chance.integer({min: 1, max: 5});
    var participantRating = chance.integer({min: 1, max: 5});
    var teachingJobs = generateRandomArray(chance.integer({min: 1, max: 5}), getJobIdMinRange(), getJobIdMaxRange());
    var participantJobs = generateRandomArray(chance.integer({min: 1, max: 5}), getJobIdMinRange(), getJobIdMaxRange());

    return {"userId" : userId , "firstName" : firstName, "lastName" : lastName, "email" : email, "password" : password, "linkedInURL" : linkedInURL, "teachingRating" : teachingRating, "participantRating" : participantRating, "teachingJobs" : teachingJobs, "participantJobs" : participantJobs};
  }

  function generateRandomUserJsonArray(n){
    users = []; // For printing to console later
    for (var i = 0; i < n; i++){
      var userJson = generateRandomUserJson();
      users.push(userJson);
      messagesRef.push(userJson);
    }
    return users;
  }

console.log("Creating users");
console.log(generateRandomUserJsonArray(5));
console.log("Successfully created users");
