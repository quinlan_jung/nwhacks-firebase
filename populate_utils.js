function getUserIdMinRange(){
	return 100;
}

function getUserIdMaxRange(){
	return 500;
}

function getJobIdMinRange(){
	return 1000;
}

function getJobIdMaxRange(){
	return 5000;
}

function generateRandomUserId(){
	return chance.integer({min: getUserIdMinRange(), max: getUserIdMaxRange()});
}

function generateRandomJobId(){
	return chance.integer({min: getJobIdMinRange(), max: getJobIdMaxRange()});
}

function generateRandomArray(length, minValue, maxValue){
	arrays = []; // For printing to console later
    for (var i = 0; i < length; i++){
      arrays.push(chance.integer({min: minValue, max: maxValue}));
    }
    return arrays;
}